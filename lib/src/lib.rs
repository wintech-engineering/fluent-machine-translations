//! A utility for rewriting [Fluent][fluent] resources by passing them through
//! machine translation.
//!
//! # Examples
//!
//! Most of the time you'll be using the high-level [`translate()`] function.
//! This takes a set of *Fluent* messages ([`Resource`]) and a function for
//! translating strings in bulk, and will try to generate a new [`Resource`]
//! with as much text translated as possible.
//!
//! ```rust
//! use fluent_machine_translations::{StringPool, MarkWithToDo};
//! use anyhow::Error;
//! use std::collections::{HashMap, HashSet};
//!
//! let src = r"
//! greeting = Hello, World!
//! missing-translation = We don't know how to translate this.
//! complex-expression = Hello, { $name }!
//! ";
//! let resource = fluent_syntax::parser::parse(src).unwrap();
//!
//! // Because the message AST works with borrowed strings, we need an arena
//! // that any new strings can be placed in to ensure they outlive the AST.
//! let string_pool = StringPool::empty();
//!
//! // When a message can't be translated, the error handling strategy will be
//! // used to figure out how to handle it.
//! //
//! // In this case, we just add a "TODO: Translate This" comment before the
//! // offending message.
//! let error_handling_strategy = MarkWithToDo::default();
//!
//! let translated = fluent_machine_translations::translate(
//!     &resource,
//!     &string_pool,
//!     hard_coded_translate,
//!     error_handling_strategy,
//! ).unwrap();
//!
//! // Print the translated resource as a string so we can inspect it
//! let got = fluent_syntax::serializer::serialize(&translated);
//! let should_be = r"greeting = Salut, tout le monde!
//! ## TODO: Translate this
//! missing-translation = We don't know how to translate this.
//! ## TODO: Translate this
//! complex-expression = Hello, { $name }!
//! ";
//! assert_eq!(got, should_be);
//!
//!
//! /// "translate" a batch of strings by looking them up in an internal table.
//! /// If we don't know how to translate a string it'll be ignored. That way
//! /// if it's important, the error-handling strategy can figure it out.
//! ///
//! /// Your implementation will probably go through Google's Cloud Translate
//! /// or a similar service.
//! fn hard_coded_translate(strings: HashSet<&str>) -> Result<HashMap<&str, String>, Error> {
//!     let known_translations = &[
//!         ("Hello, World!", "Salut, tout le monde!"),
//!     ];
//!
//!     let mut translated = HashMap::new();
//!
//!     for string in strings {
//!         let got = known_translations.iter()
//!             .find(|(original, _)| *original == string)
//!             .map(|(_original, translated)| translated);
//!         if let Some(equivalent) = got {
//!             translated.insert(string, equivalent.to_string());
//!         }
//!     }
//!
//!     Ok(translated)
//! }
//! ```
//!
//! # Error Handling
//!
//! This crate works on a best-effort basis, and as such will encounter messages
//! that can't be translated.
//!
//! The user can provide an [`ErrorHandlingStrategy`] to deal with this
//! scenarios, with common strategies being to skip the message completely
//! ([`Exclude`]) or add a "TODO" comment above the message so a human can
//! translate it manually ([`MarkWithToDo`]).
//!
//! The [`translate()`] function is deliberately naive and will only translate
//! messages containing entire strings. If a message is complex (i.e. it
//! contains placeables like `"Hello, { $name }!"`) the algorithm will bail,
//! invoking a method on [`ErrorHandlingStrategy`] with
//! [`SubstitutionFailure::NotSimple`]. This is because it is generally not
//! possible to *just* translate the bits of text we can see due to things like
//! word order.
//!
//! When a string has no translation, the [`ErrorHandlingStrategy`] will be
//! invoked with [`SubstitutionFailure::MissingTranslation`].
//!
//! [fluent]: https://www.projectfluent.org/

#![doc(
    html_favicon_url = "https://www.wintechengineering.com.au/wp-content/themes/wintech/images/favicon.png",
    html_logo_url = "https://www.wintechengineering.com.au/wp-content/themes/wintech/images/wintech-logo.svg"
)]
#![deny(
    missing_docs,
    missing_debug_implementations,
    missing_copy_implementations
)]
#![deny(unsafe_code)]

mod extract;
mod string_pool;
mod sub;

pub use extract::extract_simple_strings;
pub use string_pool::StringPool;
pub use sub::{
    substitute, ErrorHandlingStrategy, Exclude, MarkWithToDo,
    SubstitutionFailure,
};

use anyhow::Error;
use fluent_syntax::ast::*;
use std::collections::{HashMap, HashSet};

/// Try to translate a resource from one language to another.
///
/// # Note
///
/// The `bulk_translate` function passed to [`translate()`] shouldn't error
/// out if it is unable to translate a string. Instead, skip the string and let
/// the system invoke your [`ErrorHandlingStrategy`] with
/// [`SubstitutionFailure::MissingTranslation`].
pub fn translate<'ast, F, S>(
    resource: &'ast Resource<'ast>,
    string_pool: &'ast StringPool,
    bulk_translate: F,
    error_handling_strategy: S,
) -> Result<Resource<'ast>, Error>
where
    F: FnOnce(HashSet<&str>) -> Result<HashMap<&str, String>, Error>,
    S: ErrorHandlingStrategy,
{
    let strings = extract_simple_strings(resource);
    let translations = bulk_translate(strings)?;

    substitute(
        resource,
        &translations,
        string_pool,
        error_handling_strategy,
    )
}
