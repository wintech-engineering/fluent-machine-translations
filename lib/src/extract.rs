use fluent_syntax::ast::*;
use std::collections::HashSet;

/// Scans through each [`Message`] or [`Term`] in a [`Resource`] and extracts
/// any strings it can find.
///
/// Messages containing non-trivial constructs (i.e. anything other than
/// [`PatternElement::TextElement`]) will be skipped completely.
pub fn extract_simple_strings<'ast>(
    resource: &'ast Resource<'ast>,
) -> HashSet<&'ast str> {
    let mut strings = HashSet::new();
    let attributes_and_patterns =
        resource.body.iter().filter_map(as_attributes_and_pattern);

    for item in attributes_and_patterns {
        if let Some(pattern) = item.pattern {
            visit_pattern(pattern, &mut strings);
        }
        for attr in item.attributes {
            visit_attribute(attr, &mut strings);
        }
    }

    strings
}

fn visit_pattern<'ast>(
    pattern: &Pattern<'ast>,
    strings: &mut HashSet<&'ast str>,
) {
    for elem in &pattern.elements {
        if let PatternElement::TextElement(text) = elem {
            strings.insert(text);
        }
    }
}

fn visit_attribute<'ast>(
    attr: &Attribute<'ast>,
    strings: &mut HashSet<&'ast str>,
) {
    visit_pattern(&attr.value, strings);
}

struct AttributesAndPattern<'ast> {
    pattern: Option<&'ast Pattern<'ast>>,
    attributes: &'ast [Attribute<'ast>],
}

fn as_attributes_and_pattern<'ast>(
    entry: &'ast ResourceEntry<'ast>,
) -> Option<AttributesAndPattern<'ast>> {
    match entry {
        ResourceEntry::Entry(Entry::Message(Message {
            value: None,
            attributes,
            ..
        })) if are_simple_attributes(attributes) => {
            Some(AttributesAndPattern {
                pattern: None,
                attributes,
            })
        },

        ResourceEntry::Entry(Entry::Message(Message {
            value: Some(value),
            attributes,
            ..
        }))
        | ResourceEntry::Entry(Entry::Term(Term {
            value, attributes, ..
        })) if is_simple_pattern(value)
            && are_simple_attributes(attributes) =>
        {
            Some(AttributesAndPattern {
                pattern: Some(value),
                attributes,
            })
        },

        _ => None,
    }
}

fn is_simple_pattern(pat: &Pattern<'_>) -> bool {
    pat.elements
        .iter()
        .all(|elem| matches!(elem, PatternElement::TextElement(_)))
}

fn is_simple_attribute(attr: &Attribute<'_>) -> bool {
    is_simple_pattern(&attr.value)
}

fn are_simple_attributes(attrs: &[Attribute<'_>]) -> bool {
    attrs.iter().all(is_simple_attribute)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn extract_strings_from_a_simple_message() {
        let src = "foo = Hello, World!";
        let resource = fluent_syntax::parser::parse(src).unwrap();

        let got = extract_simple_strings(&resource);

        assert_eq!(got.len(), 1);
        assert!(got.contains(&"Hello, World!"));
    }

    #[test]
    fn ignore_messages_containing_a_placeable() {
        let src = "foo = Hello, { $name }!";
        let resource = fluent_syntax::parser::parse(src).unwrap();

        let got = extract_simple_strings(&resource);

        assert!(got.is_empty());
    }
}
