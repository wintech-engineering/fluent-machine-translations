use crate::string_pool::StringPool;
use anyhow::Error;
use fluent_syntax::ast::*;
use std::{
    collections::HashMap,
    fmt::{self, Display, Formatter},
};

/// Try to translate a [`Resource`] by substituting in translations from a
/// translations table.
///
/// The [`ErrorHandlingStrategy`] will be used when a message can't be
/// translated. Commonly used strategies are [`MarkWithToDo`] or [`Exclude`].
pub fn substitute<'t, 'ast: 't, S>(
    resource: &'ast Resource<'ast>,
    translations: &'t HashMap<&'t str, String>,
    string_pool: &'ast StringPool,
    mut strategy: S,
) -> Result<Resource<'ast>, Error>
where
    S: ErrorHandlingStrategy,
{
    let mut body: Vec<ResourceEntry<'ast>> = Vec::new();
    let sub = Sub {
        translations,
        string_pool,
    };

    for entry in &resource.body {
        match entry {
            ResourceEntry::Entry(Entry::Message(msg)) => {
                match sub.message(msg) {
                    Ok(new_msg) => {
                        body.push(ResourceEntry::Entry(Entry::Message(new_msg)))
                    },
                    Err(e) => {
                        strategy
                            .handle_message_not_translated(msg, e, &mut body)?;
                    },
                }
            },
            ResourceEntry::Entry(Entry::Term(term)) => match sub.term(term) {
                Ok(new_term) => {
                    body.push(ResourceEntry::Entry(Entry::Term(new_term)))
                },
                Err(e) => {
                    strategy.handle_term_not_translated(term, e, &mut body)?;
                },
            },
            other => body.push(other.clone()),
        }
    }

    Ok(Resource { body })
}

struct Sub<'t, 'a: 't> {
    translations: &'t HashMap<&'t str, String>,
    string_pool: &'a StringPool,
}

impl<'t, 'a: 't> Sub<'t, 'a> {
    fn message(
        &self,
        msg: &Message<'a>,
    ) -> Result<Message<'a>, SubstitutionFailure> {
        let value = match msg.value {
            Some(ref value) => Some(self.pattern(value)?),
            None => None,
        };
        let attributes = self.attributes(&msg.attributes)?;

        Ok(Message {
            id: msg.id.clone(),
            comment: msg.comment.clone(),
            value,
            attributes,
        })
    }

    fn term(&self, term: &Term<'a>) -> Result<Term<'a>, SubstitutionFailure> {
        let value = self.pattern(&term.value)?;
        let attributes = self.attributes(&term.attributes)?;

        Ok(Term {
            id: term.id.clone(),
            comment: term.comment.clone(),
            value,
            attributes,
        })
    }

    fn pattern(
        &self,
        pattern: &Pattern<'a>,
    ) -> Result<Pattern<'a>, SubstitutionFailure> {
        let mut elements = Vec::new();

        for element in &pattern.elements {
            elements.push(self.element(element)?);
        }

        Ok(Pattern { elements })
    }

    fn element(
        &self,
        element: &PatternElement<'a>,
    ) -> Result<PatternElement<'a>, SubstitutionFailure> {
        match element {
            PatternElement::TextElement(text) => {
                match self.translations.get(text) {
                    Some(translation) => {
                        let translation = self.string_pool.intern(translation);
                        Ok(PatternElement::TextElement(translation))
                    },
                    None => Err(SubstitutionFailure::MissingTranslation {
                        original: text.to_string(),
                    }),
                }
            },
            PatternElement::Placeable(_) => Err(SubstitutionFailure::NotSimple),
        }
    }

    fn attributes(
        &self,
        attrs: &[Attribute<'a>],
    ) -> Result<Vec<Attribute<'a>>, SubstitutionFailure> {
        let mut attributes = Vec::new();

        for attr in attrs {
            attributes.push(self.attribute(attr)?);
        }

        Ok(attributes)
    }

    fn attribute(
        &self,
        attribute: &Attribute<'a>,
    ) -> Result<Attribute<'a>, SubstitutionFailure> {
        Ok(Attribute {
            id: attribute.id.clone(),
            value: self.pattern(&attribute.value)?,
        })
    }
}

/// The callback invoked to handle a failed substitution (e.g. due to missing
/// translations or the expression being too complex).
pub trait ErrorHandlingStrategy {
    /// A [`Message`] couldn't be translated.
    ///
    /// The handler is given the original [`Message`], reason for failure, and
    /// a pointer to the [`Resource::body`] we're building up.
    fn handle_message_not_translated<'ast>(
        &mut self,
        msg: &Message<'ast>,
        _failure: SubstitutionFailure,
        body: &mut Vec<ResourceEntry<'ast>>,
    ) -> Result<(), Error> {
        body.push(ResourceEntry::Entry(Entry::Message(msg.clone())));
        Ok(())
    }

    /// A [`Term`] couldn't be translated.
    ///
    /// The handler is given the original [`Term`], reason for failure, and
    /// a pointer to the [`Resource::body`] we're building up.
    fn handle_term_not_translated<'ast>(
        &mut self,
        term: &Term<'ast>,
        _failure: SubstitutionFailure,
        body: &mut Vec<ResourceEntry<'ast>>,
    ) -> Result<(), Error> {
        body.push(ResourceEntry::Entry(Entry::Term(term.clone())));
        Ok(())
    }
}

impl<'a, E: ErrorHandlingStrategy + ?Sized> ErrorHandlingStrategy
    for &'a mut E
{
    fn handle_message_not_translated<'ast>(
        &mut self,
        msg: &Message<'ast>,
        failure: SubstitutionFailure,
        body: &mut Vec<ResourceEntry<'ast>>,
    ) -> Result<(), Error> {
        (**self).handle_message_not_translated(msg, failure, body)
    }

    fn handle_term_not_translated<'ast>(
        &mut self,
        term: &Term<'ast>,
        failure: SubstitutionFailure,
        body: &mut Vec<ResourceEntry<'ast>>,
    ) -> Result<(), Error> {
        (**self).handle_term_not_translated(term, failure, body)
    }
}

/// The reason a message can't be translated.
#[derive(Debug, Clone, PartialEq)]
#[non_exhaustive]
pub enum SubstitutionFailure {
    /// There is no translation for some text.
    MissingTranslation {
        /// The text that doesn't have a translation.
        original: String,
    },
    /// The message is too complex to reliably translate.
    NotSimple,
}

impl Display for SubstitutionFailure {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            SubstitutionFailure::MissingTranslation { original } => {
                write!(f, "Unable to translate \"{}\"", original)
            },
            SubstitutionFailure::NotSimple => {
                write!(f, "The expression was too complex")
            },
        }
    }
}

impl std::error::Error for SubstitutionFailure {}

/// An [`ErrorHandlingStrategy`] that appends a comment to a message and passes
/// it back otherwise unchanged.
///
/// The default comment is something like `"TODO: Translate this"`.
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct MarkWithToDo {
    // Note: we use `&'static str` here because the AST uses borrowed strings,
    // and using `&'a str` would give the user tricky lifetime messages when
    // trying to use it as a strategy.
    message: &'static str,
}
impl MarkWithToDo {
    /// Create a new [`MarkWithToDo`].
    pub fn new(message: &'static str) -> Self { MarkWithToDo { message } }

    fn with_message<'ast>(
        &self,
        comment: Option<Comment<'ast>>,
    ) -> Comment<'ast> {
        match comment {
            Some(mut comment) => {
                match comment {
                    Comment::Comment { ref mut content }
                    | Comment::GroupComment { ref mut content }
                    | Comment::ResourceComment { ref mut content } => {
                        content.push(self.message)
                    },
                }

                comment
            },
            None => Comment::Comment {
                content: vec![self.message],
            },
        }
    }
}

impl Default for MarkWithToDo {
    fn default() -> Self { MarkWithToDo::new("TODO: Translate this") }
}

impl ErrorHandlingStrategy for MarkWithToDo {
    fn handle_message_not_translated<'ast>(
        &mut self,
        msg: &Message<'ast>,
        _failure: SubstitutionFailure,
        body: &mut Vec<ResourceEntry<'ast>>,
    ) -> Result<(), Error> {
        let mut msg = msg.clone();
        msg.comment = Some(self.with_message(msg.comment.take()));
        body.push(ResourceEntry::Entry(Entry::Message(msg)));

        Ok(())
    }

    fn handle_term_not_translated<'ast>(
        &mut self,
        term: &Term<'ast>,
        _failure: SubstitutionFailure,
        body: &mut Vec<ResourceEntry<'ast>>,
    ) -> Result<(), Error> {
        let mut term = term.clone();
        term.comment = Some(self.with_message(term.comment.take()));
        body.push(ResourceEntry::Entry(Entry::Term(term)));

        Ok(())
    }
}

/// An [`ErrorHandlingStrategy`] which will ignore exclude untranslated messages
/// from the final resource (e.g. to allow locale fallbacks to display an
/// alternate message).
#[derive(Debug, Copy, Clone, Default)]
pub struct Exclude;

impl ErrorHandlingStrategy for Exclude {
    fn handle_message_not_translated<'ast>(
        &mut self,
        _msg: &Message<'ast>,
        _failure: SubstitutionFailure,
        _body: &mut Vec<ResourceEntry<'ast>>,
    ) -> Result<(), Error> {
        Ok(())
    }

    fn handle_term_not_translated<'ast>(
        &mut self,
        _term: &Term<'ast>,
        _failure: SubstitutionFailure,
        _body: &mut Vec<ResourceEntry<'ast>>,
    ) -> Result<(), Error> {
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[derive(Debug, Default, Clone, PartialEq)]
    struct Counter {
        bad_messages: Vec<SubstitutionFailure>,
        bad_terms: Vec<SubstitutionFailure>,
    }

    impl ErrorHandlingStrategy for Counter {
        fn handle_message_not_translated<'ast>(
            &mut self,
            _msg: &Message<'ast>,
            failure: SubstitutionFailure,
            _body: &mut Vec<ResourceEntry<'ast>>,
        ) -> Result<(), Error> {
            self.bad_messages.push(failure);
            Ok(())
        }

        fn handle_term_not_translated<'ast>(
            &mut self,
            _term: &Term<'ast>,
            failure: SubstitutionFailure,
            _body: &mut Vec<ResourceEntry<'ast>>,
        ) -> Result<(), Error> {
            self.bad_terms.push(failure);
            Ok(())
        }
    }

    #[test]
    fn invoke_strategy_on_complex_expressions() {
        let src = "msg = Hello, { $name }!";
        let resource = fluent_syntax::parser::parse(src).unwrap();
        let translations = vec![("Hello, ", String::from("Salut, "))]
            .into_iter()
            .collect();
        let string_pool = StringPool::empty();
        let mut counter = Counter::default();

        let got =
            substitute(&resource, &translations, &string_pool, &mut counter)
                .unwrap();

        assert!(got.body.is_empty());
        assert_eq!(counter.bad_messages.len(), 1);
        assert_eq!(counter.bad_messages[0], SubstitutionFailure::NotSimple);
        assert!(counter.bad_terms.is_empty());
    }

    #[test]
    fn invoke_strategy_on_missing_translation() {
        let src = "msg = Hello, World!";
        let resource = fluent_syntax::parser::parse(src).unwrap();
        let translations = HashMap::new();
        let string_pool = StringPool::empty();
        let mut counter = Counter::default();

        let got =
            substitute(&resource, &translations, &string_pool, &mut counter)
                .unwrap();

        assert!(got.body.is_empty());
        assert_eq!(counter.bad_messages.len(), 1);
        assert_eq!(
            counter.bad_messages[0],
            SubstitutionFailure::MissingTranslation {
                original: String::from("Hello, World!"),
            }
        );
        assert!(counter.bad_terms.is_empty());
    }

    macro_rules! substitution_test {
        ($name:ident,
            translate $input:expr => $should_be:expr,
            using $strategy:expr $(,)?
        ) => {
            substitution_test!($name, translate $input => $should_be, using $strategy, and);
        };
        ($name:ident,
            translate $input:expr => $should_be:expr,
            using $strategy:expr,
            and $( $key:expr => $value:expr),*
            $(,)?
        ) => {
            #[test]
            #[allow(unused_mut)]
            fn $name() {
                let resource = fluent_syntax::parser::parse($input).unwrap();
                let mut translations = HashMap::new();
                $(
                    translations.insert($key, $value.to_string());
                )*
                let string_pool = StringPool::empty();

                let got = substitute(
                    &resource,
                    &translations,
                    &string_pool,
                    $strategy,
                )
                .unwrap();

                let serialized = fluent_syntax::serializer::serialize(&got);
                assert_eq!(serialized, $should_be);
            }
        };
    }

    substitution_test!(
        translate_simple_message,
        translate "msg = Hello, World!" => "msg = Salut, tout le monde!\n",
        using Exclude,
        and "Hello, World!" => "Salut, tout le monde!",
    );
    substitution_test!(
        mark_with_todo_comment,
        translate "msg = Hello, World!" => "# TODO: Translate this\nmsg = Hello, World!\n",
        using MarkWithToDo::default()
    );
    substitution_test!(
        append_todo_to_existing_comment,
        translate "# Existing Comment\nmsg = Hello, World!"
        => "# Existing Comment\n# TODO: Translate this\nmsg = Hello, World!\n",
        using MarkWithToDo::default()
    );
    substitution_test!(
        exclude_messages_with_missing_translations,
        translate "msg = Hello, World!\n\nother = Oops...\n" => "",
        using Exclude
    );
}
