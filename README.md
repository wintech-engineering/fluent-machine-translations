# fluent-machine-translations

[![pipeline status](https://gitlab.com/wintech-engineering/fluent-machine-translations/badges/master/pipeline.svg)](https://gitlab.com/wintech-engineering/fluent-machine-translations/pipelines)

([API Docs][docs])

A utility for passing [Fluent][fluent] files through Google Translate.

## Getting Started

The command-line application will need to be installed from source.

```console
$ cargo install --git https://gitlab.com/wintech-engineering/fluent-machine-translations \
    --bin fluent-machine-translations-cli
```

The `fluent-machine-translations-cli` program works by using Google's
[Cloud Translate][translate] API. Make sure to generate an [API Key][key] for
authenticating with the service before continuing.

Now you can translate a Fluent file. For example, if you wanted to translate
`en.ftl` to German and save it as `de.ftl`, you would execute the following:

```console
$ fluent-machine-translations-cli --key $API_KEY --input en.ftl --output de.ftl --output-language de
Starting Args {
    input: ".en.ftl",
    output: Some(
        "de.ftl",
    ),
    input_language: None,
    output_language: "de",
    api_key: "...",
}
Translating 534 strings across 5 batches (8846 characters)
```

when specifying a language, you must specify a supported [iso-639-1][iso-639]
identifier. The list of supported languages is in the [Cloud Translate
docs][languages].

To use the crate as a library (e.g. because you want to customise how
translations are done) you'll need to add the `fluent-machine-translations`
crate as a `git` dependency.

```toml
[dependencies]
fluent-machine-translations = { git = "https://gitlab.com/wintech-engineering/fluent-machine-translations" }

# Use a custom fork of fluent-rs while waiting for upstream to merge the
# following PRs:
#   - #184
#   - #185
#   - #186
fluent-syntax = { git = "https://github.com/Michael-F-Bryan/fluent-rs", branch = "needed-for-wintech" }
```

For examples on using the crate, see [the API Docs][docs].

## License

This project is licensed under either of

 * Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE.md) or
   http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT.md) or
   http://opensource.org/licenses/MIT)

at your option.

It is recommended to always use [cargo-crev][crev] to verify the
trustworthiness of each of your dependencies, including this one.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the Apache-2.0
license, shall be dual licensed as above, without any additional terms or
conditions.

The intent of this crate is to be free of soundness bugs. The developers will
do their best to avoid them, and welcome help in analysing and fixing them.

[docs]: https://wintech-engineering.gitlab.io/fluent-machine-translations
[crev]: https://github.com/crev-dev/cargo-crev
[fluent]: https://www.projectfluent.org/
[translate]: https://cloud.google.com/translate
[key]: https://cloud.google.com/docs/authentication/api-keys
[languages]: https://cloud.google.com/translate/docs/languages
[iso-639]: https://en.wikipedia.org/wiki/ISO_639-1
