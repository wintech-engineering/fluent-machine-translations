use anyhow::{Context, Error};
use codespan::{FileId, Files};
use codespan_reporting::{
    diagnostic::{Diagnostic, Label},
    term::{
        self,
        termcolor::{ColorChoice, StandardStream},
        Config,
    },
};
use fluent_machine_translations::{MarkWithToDo, StringPool};
use fluent_syntax::parser::errors::ParserError;
use rayon::prelude::*;
use reqwest::blocking::Client;
use std::{
    collections::HashMap,
    path::{Path, PathBuf},
};
use structopt::StructOpt;

const USER_AGENT: &str =
    concat!(env!("CARGO_PKG_NAME"), ".", env!("CARGO_PKG_VERSION"));

fn main() -> Result<(), Error> {
    let args = Args::from_args();
    eprintln!("Starting {:#?}", args);

    let src = std::fs::read_to_string(&args.input).with_context(|| {
        format!("Unable to read \"{}\"", args.input.display())
    })?;
    let resource = match fluent_syntax::parser::parse(&src) {
        Ok(res) => res,
        Err((_, errors)) => {
            print_parse_errors(&args.input, &src, &errors)?;
            anyhow::bail!("Aborting due to parse errors");
        },
    };

    let string_pool = StringPool::empty();
    let client = Client::builder()
        .user_agent(USER_AGENT)
        .build()
        .context("Unable to configure the web client")?;

    let got = fluent_machine_translations::translate(
        &resource,
        &string_pool,
        |strings| {
            translate(
                &client,
                strings,
                args.input_language.as_deref(),
                &args.output_language,
                &args.api_key,
            )
        },
        MarkWithToDo::default(),
    )?;

    let translated = fluent_syntax::serializer::serialize(&got);

    match args.output {
        Some(output) => std::fs::write(&output, translated.as_bytes())
            .with_context(|| {
                format!(
                    "Unable to write translated text to \"{}\"",
                    output.display()
                )
            })?,
        None => println!("{}", translated),
    }

    Ok(())
}

#[derive(Debug, StructOpt)]
struct Args {
    #[structopt(
        short = "i",
        long = "input",
        parse(from_os_str),
        help = "The file to translate"
    )]
    input: PathBuf,
    #[structopt(
        short = "o",
        long = "output",
        parse(from_os_str),
        help = "Where to write translated messages to"
    )]
    output: Option<PathBuf>,
    #[structopt(
        long = "input-language",
        help = "The input language as an ISO-639-1 code (leave blank to auto-detect)"
    )]
    input_language: Option<String>,
    #[structopt(
        long = "output-language",
        help = "The destination language as an ISO-639-1 code"
    )]
    output_language: String,
    #[structopt(short = "k", long = "key", help = "The API key to use", env)]
    api_key: String,
}

fn print_parse_errors(
    path: &Path,
    src: &str,
    errors: &[ParserError],
) -> Result<(), Error> {
    let mut files = Files::new();
    let id = files.add(path, src);

    let mut writer = StandardStream::stderr(ColorChoice::Auto);
    let cfg = Config::default();

    for error in errors {
        let diag = as_diagnostic(error, id);
        term::emit(&mut writer, &cfg, &files, &diag)?;
    }

    Ok(())
}

fn as_diagnostic(error: &ParserError, file_id: FileId) -> Diagnostic<FileId> {
    let (start, end) = error.pos;
    Diagnostic::error().with_labels(vec![Label::primary(file_id, start..end)
        .with_message(error.kind.to_string())])
}

fn translate<'src, S>(
    client: &Client,
    strings: S,
    input_language: Option<&str>,
    output_language: &str,
    api_key: &str,
) -> Result<HashMap<&'src str, String>, Error>
where
    S: IntoIterator<Item = &'src str>,
{
    let buckets = into_buckets(strings);
    let stats = buckets
        .iter()
        .fold(Stats::default(), |stats, bucket| stats.merge(bucket));
    eprintln!(
        "Translating {} strings across {} batches ({} characters)",
        stats.strings, stats.buckets, stats.characters
    );

    buckets
        .par_iter()
        .try_fold(HashMap::new, |translations, chunk| {
            let got = batch_translate(
                client,
                chunk,
                input_language,
                output_language,
                api_key,
            )?;
            Ok(merge(translations, got))
        })
        .try_reduce(HashMap::new, |left, right| Ok(merge(left, right)))
}

/// The TranslateText endpoint has an upper limit on the number of strings per
/// request and characters per request. This splits the input stream up into
/// appropriately sized buckets.
fn into_buckets<'a, S>(strings: S) -> Vec<Vec<&'a str>>
where
    S: IntoIterator<Item = &'a str>,
{
    const MAX_CHARACTERS_PER_BUCKET: usize = 20000;
    const MAX_STRINGS_PER_BUCKET: usize = 128;

    let mut buckets = Vec::new();
    let mut current_bucket = Vec::new();
    let mut current_character_count = 0;

    for string in strings {
        if current_character_count + string.len() >= MAX_CHARACTERS_PER_BUCKET
            || current_bucket.len() >= MAX_STRINGS_PER_BUCKET
        {
            buckets.push(current_bucket);
            current_bucket = Vec::new();
            current_character_count = 0;
        }

        current_bucket.push(string);
        current_character_count += string.len();
    }

    if !current_bucket.is_empty() {
        buckets.push(current_bucket)
    }

    buckets
}

fn merge<E, T>(mut left: E, right: E) -> E
where
    E: IntoIterator<Item = T> + Extend<T>,
{
    left.extend(right.into_iter());
    left
}

fn batch_translate<'src>(
    client: &Client,
    strings: &[&'src str],
    input_language: Option<&str>,
    output_language: &str,
    api_key: &str,
) -> Result<HashMap<&'src str, String>, Error> {
    // See https://cloud.google.com/translate/docs/reference/rest/v2/translate
    const URL: &'static str =
        "https://translation.googleapis.com/language/translate/v2";

    let request = Request {
        strings,
        source: input_language,
        target: output_language,
        format: "text",
    };

    let response: Response = client
        .post(URL)
        .json(&request)
        .query(&[("key", api_key)])
        .send()?
        .error_for_status()?
        .json()
        .context("Unable to deserialize the response")?;

    let translations = response
        .data
        .translations
        .into_iter()
        .map(|t| t.translated_text);

    Ok(strings.into_iter().map(|s| *s).zip(translations).collect())
}

#[derive(Debug, serde::Serialize)]
struct Request<'a> {
    #[serde(rename = "q")]
    strings: &'a [&'a str],
    target: &'a str,
    source: Option<&'a str>,
    format: &'a str,
}

#[derive(Debug, serde::Deserialize)]
struct Response {
    data: Data,
}

#[derive(Debug, serde::Deserialize)]
struct Data {
    translations: Vec<Translation>,
}

#[derive(Debug, serde::Deserialize)]
#[serde(rename_all = "camelCase")]
struct Translation {
    translated_text: String,
    detected_source_language: String,
}

#[derive(Debug, Default)]
struct Stats {
    buckets: usize,
    strings: usize,
    characters: usize,
}

impl Stats {
    fn merge(mut self, bucket: &[&str]) -> Self {
        for string in bucket {
            self.strings += 1;
            self.characters += string.chars().count();
        }
        self.buckets += 1;
        self
    }
}
